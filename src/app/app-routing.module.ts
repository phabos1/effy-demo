import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { PagePersonalInformationsComponent } from './components/pages/page-personal-informations/page-personal-informations.component';
import { PageProjectInformationsComponent } from './components/pages/page-project-informations/page-project-informations.component';
import { PageSumupInformationsComponent } from './components/pages/page-sumup-informations/page-sumup-informations.component';
import {
  PERSONAL_INFOS_PATH,
  PROJECT_INFOS_PATH,
  SUMUP_INFOS_PATH,
} from './models/constants';
import { RouteGuardService } from './services/route-guard.service';

const routes: Routes = [
  {
    path: PERSONAL_INFOS_PATH,
    component: PagePersonalInformationsComponent,
  },
  {
    path: PROJECT_INFOS_PATH,
    component: PageProjectInformationsComponent,
    canActivate: [RouteGuardService],
  },
  {
    path: SUMUP_INFOS_PATH,
    component: PageSumupInformationsComponent,
    canActivate: [RouteGuardService],
  },
  { path: '', redirectTo: `/${PERSONAL_INFOS_PATH}`, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
