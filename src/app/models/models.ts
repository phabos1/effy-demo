type Nullable<T> = T | null;

export type Type = 'tenant' | 'owner';

export type Civility = 'm' | 'mme';

export enum Steps {
  personalInfos = 'personalInfos',
  projectInfos = 'projectInfos',
  sumpup = 'sumpup',
}

export type FormSteps = Steps.personalInfos | Steps.projectInfos;
export type FormRegistrationValue = Nullable<
  PersonalInformationFormValue | ProjectInformationFormValue
>;

export type FormRegistrationValues = {
  [Steps.personalInfos]: Nullable<PersonalInformationFormValue>;
  [Steps.projectInfos]: Nullable<ProjectInformationFormValue>;
};

export interface PersonalInformationFormValue {
  civility: Civility;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}

export interface ProjectInformationFormValue {
  area: string;
  incomes: number;
  persons: number;
  type: Type;
}

export interface computedValues {
  projectCost: number;
  effyHelpAmount: number;
}
