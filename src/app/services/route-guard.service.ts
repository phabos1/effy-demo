import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import {
  PERSONAL_INFOS_PATH,
  PROJECT_INFOS_PATH,
  SUMUP_INFOS_PATH,
} from '../models/constants';
import { RegistrationFormService } from './registration-form.service';

@Injectable()
export class RouteGuardService implements CanActivate {
  constructor(
    private router: Router,
    private registerFormService: RegistrationFormService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (
      state.url === `/${PROJECT_INFOS_PATH}` &&
      this.registerFormService.registrationFormValues.personalInfos === null
    ) {
      this.router.navigate([PERSONAL_INFOS_PATH]);
      return false;
    }

    if (
      state.url === `/${SUMUP_INFOS_PATH}` &&
      this.registerFormService.registrationFormValues.personalInfos === null &&
      this.registerFormService.registrationFormValues.projectInfos === null
    ) {
      this.router.navigate([PERSONAL_INFOS_PATH]);
      return false;
    }

    return true;
  }
}
