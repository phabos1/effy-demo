import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  computedValues,
  FormRegistrationValues,
  FormSteps,
  Steps,
} from '../models/models';

@Injectable({
  providedIn: 'root',
})
export class RegistrationFormService {
  private _currentStep: BehaviorSubject<Steps> = new BehaviorSubject<Steps>(
    Steps.personalInfos
  );
  private _registrationFormValues: FormRegistrationValues = {
    [Steps.personalInfos]: null,
    [Steps.projectInfos]: null,
  };

  constructor() {}

  get currentStep(): BehaviorSubject<Steps> {
    return this._currentStep;
  }

  get registrationFormValues(): FormRegistrationValues {
    return this._registrationFormValues;
  }

  setCurrentStep(step: Steps): void {
    this._currentStep.next(step);
  }

  setRegistrationFormValue(step: FormSteps, formValues: any): void {
    this._registrationFormValues[step] = formValues;
  }

  aggregateFormValues(): computedValues {
    const area: number =
      +this._registrationFormValues[Steps.projectInfos]!.area;
    const incomes: number =
      +this._registrationFormValues[Steps.projectInfos]!.incomes;
    const persons: number =
      +this._registrationFormValues[Steps.projectInfos]!.persons;

    const projectCost = area * 80;
    const effyHelpAmount = projectCost * 0.75 - (incomes / persons) * 0.15;

    return {
      projectCost,
      effyHelpAmount,
    };
  }
}
