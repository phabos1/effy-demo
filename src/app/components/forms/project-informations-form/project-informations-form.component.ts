import { Options } from '@angular-slider/ngx-slider';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectInformationFormValue } from 'src/app/models/models';

@Component({
  selector: 'app-project-informations-form',
  templateUrl: './project-informations-form.component.html',
  styleUrls: ['./project-informations-form.component.css'],
})
export class ProjectInformationsFormComponent implements OnInit {
  @Output() projectInfoEvent = new EventEmitter<ProjectInformationFormValue>();
  options: Options = {
    floor: 10,
    ceil: 100,
    translate: (value: number): string => {
      return `${value}k`;
    },
  };
  projectInformationsForm: FormGroup = this.fb.group({
    type: ['', Validators.required],
    persons: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern('[1-9][0-9]?'),
      ]),
    ],
    incomes: [55, Validators.required],
    area: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern('[1-9]([0-9]{1,2})?'),
      ]),
    ],
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  onSubmit(): void {
    // May not be needed as the sumit button is disabled when form is not valid...
    if (!this.projectInformationsForm.valid) {
      return;
    }

    const formValues = this.projectInformationsForm.value;
    const incomes = `${formValues.incomes}000`;

    this.projectInfoEvent.emit({
      ...this.projectInformationsForm.value,
      incomes,
    });
  }

  trackByFn(i: number) {
    return i;
  }
}
