import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectInformationsFormComponent } from './project-informations-form.component';

describe('ProjectInformationsFormComponent', () => {
  let component: ProjectInformationsFormComponent;
  let fixture: ComponentFixture<ProjectInformationsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectInformationsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectInformationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
