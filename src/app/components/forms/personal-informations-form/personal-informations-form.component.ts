import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Civility, PersonalInformationFormValue } from 'src/app/models/models';

@Component({
  selector: 'app-personal-informations-form',
  templateUrl: './personal-informations-form.component.html',
  styleUrls: ['./personal-informations-form.component.css'],
})
export class PersonalInformationsFormComponent implements OnInit {
  @Output() personalInfoEvent =
    new EventEmitter<PersonalInformationFormValue>();
  personalInformationsForm: FormGroup = this.fb.group({
    civility: ['', Validators.required],
    firstName: [
      '',
      Validators.compose([Validators.required, Validators.minLength(3)]),
    ],
    lastName: [
      '',
      Validators.compose([Validators.required, Validators.minLength(3)]),
    ],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    phone: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern('([0-9][0-9] ){4}[0-9][0-9]'),
      ]),
    ],
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  onSubmit(): void {
    // May not be needed as the sumit button is disabled when form is not valid...
    if (!this.personalInformationsForm.valid) {
      return;
    }
    this.personalInfoEvent.emit(this.personalInformationsForm.value);
  }
}
