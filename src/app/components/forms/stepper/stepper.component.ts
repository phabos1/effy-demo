import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Steps } from 'src/app/models/models';
import { RegistrationFormService } from 'src/app/services/registration-form.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css'],
})
export class StepperComponent implements OnInit, OnDestroy {
  currentStep: Steps = Steps.personalInfos;
  sub: Subscription;

  constructor(private registerFormService: RegistrationFormService) {
    this.sub = this.registerFormService.currentStep.subscribe(
      (currentStep: Steps) => {
        this.currentStep = currentStep;
      }
    );
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
