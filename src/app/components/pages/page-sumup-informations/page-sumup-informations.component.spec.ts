import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSumupInformationsComponent } from './page-sumup-informations.component';

describe('PageSumupInformationsComponent', () => {
  let component: PageSumupInformationsComponent;
  let fixture: ComponentFixture<PageSumupInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSumupInformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSumupInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
