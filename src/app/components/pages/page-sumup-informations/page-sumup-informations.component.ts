import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PERSONAL_INFOS_PATH } from 'src/app/models/constants';
import {
  Civility,
  computedValues,
  FormRegistrationValues,
  Steps,
  Type,
} from 'src/app/models/models';
import { RegistrationFormService } from 'src/app/services/registration-form.service';

@Component({
  selector: 'app-page-sumup-informations',
  templateUrl: './page-sumup-informations.component.html',
  styleUrls: ['./page-sumup-informations.component.css'],
})
export class PageSumupInformationsComponent implements OnInit {
  formValues: FormRegistrationValues;
  aggregateValues: computedValues;
  constructor(
    private registrationFormService: RegistrationFormService,
    private router: Router
  ) {
    this.formValues = this.registrationFormService.registrationFormValues;
    this.aggregateValues = this.registrationFormService.aggregateFormValues();
  }

  ngOnInit(): void {}

  retry(event: MouseEvent): void {
    event.preventDefault();
    this.router.navigate([PERSONAL_INFOS_PATH]);
  }
}
