import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  PERSONAL_INFOS_PATH,
  SUMUP_INFOS_PATH,
} from 'src/app/models/constants';
import { ProjectInformationFormValue, Steps } from 'src/app/models/models';
import { RegistrationFormService } from 'src/app/services/registration-form.service';

@Component({
  selector: 'app-page-project-informations',
  templateUrl: './page-project-informations.component.html',
  styleUrls: ['./page-project-informations.component.css'],
})
export class PageProjectInformationsComponent implements OnInit {
  constructor(
    private router: Router,
    private registrationFormService: RegistrationFormService
  ) {}

  ngOnInit(): void {}

  onProjectInfoEvent(event: ProjectInformationFormValue): void {
    this.registrationFormService.setRegistrationFormValue(
      Steps.projectInfos,
      event
    );
    this.registrationFormService.setCurrentStep(Steps.sumpup);
    this.router.navigate([SUMUP_INFOS_PATH]);
  }
}
