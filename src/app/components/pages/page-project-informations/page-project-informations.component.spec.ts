import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageProjectInformationsComponent } from './page-project-informations.component';

describe('PageProjectInformationsComponent', () => {
  let component: PageProjectInformationsComponent;
  let fixture: ComponentFixture<PageProjectInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageProjectInformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageProjectInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
