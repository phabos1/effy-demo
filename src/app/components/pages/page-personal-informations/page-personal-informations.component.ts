import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PROJECT_INFOS_PATH } from 'src/app/models/constants';
import { PersonalInformationFormValue, Steps } from 'src/app/models/models';
import { RegistrationFormService } from 'src/app/services/registration-form.service';

@Component({
  selector: 'app-page-personal-informations',
  templateUrl: './page-personal-informations.component.html',
  styleUrls: ['./page-personal-informations.component.css'],
})
export class PagePersonalInformationsComponent implements OnInit {
  constructor(
    private router: Router,
    private registrationFormService: RegistrationFormService
  ) {}

  ngOnInit(): void {}

  onPersonalInfoEvent(event: PersonalInformationFormValue): void {
    this.registrationFormService.setRegistrationFormValue(
      Steps.personalInfos,
      event
    );
    this.registrationFormService.setCurrentStep(Steps.projectInfos);
    this.router.navigate([PROJECT_INFOS_PATH]);
  }
}
