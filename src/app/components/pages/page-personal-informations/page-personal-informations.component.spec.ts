import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePersonalInformationsComponent } from './page-personal-informations.component';

describe('PagePersonalInformationsComponent', () => {
  let component: PagePersonalInformationsComponent;
  let fixture: ComponentFixture<PagePersonalInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PagePersonalInformationsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePersonalInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
