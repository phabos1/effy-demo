import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InvalidFormClassDirective } from './directives/invalid-form-class.directive';
import { ProjectInformationsFormComponent } from './components/forms/project-informations-form/project-informations-form.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { PageSumupInformationsComponent } from './components/pages/page-sumup-informations/page-sumup-informations.component';
import { PageProjectInformationsComponent } from './components/pages/page-project-informations/page-project-informations.component';
import { PagePersonalInformationsComponent } from './components/pages/page-personal-informations/page-personal-informations.component';
import { PersonalInformationsFormComponent } from './components/forms/personal-informations-form/personal-informations-form.component';
import { CivilityPipe } from './pipes/civility.pipe';
import { RouteGuardService } from './services/route-guard.service';
import { StepperComponent } from './components/forms/stepper/stepper.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    PageSumupInformationsComponent,
    PageProjectInformationsComponent,
    PagePersonalInformationsComponent,
    PersonalInformationsFormComponent,
    InvalidFormClassDirective,
    ProjectInformationsFormComponent,
    CivilityPipe,
    StepperComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxSliderModule,
  ],
  providers: [RouteGuardService],
  bootstrap: [AppComponent],
})
export class AppModule {}
