import { Directive, HostBinding } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appInvalidFormClass]',
})
export class InvalidFormClassDirective {
  @HostBinding('class.is-invalid') get isInvalid() {
    return (
      this.ngControl.invalid && (this.ngControl.dirty || this.ngControl.touched)
    );
  }

  constructor(private ngControl: NgControl) {}
}
