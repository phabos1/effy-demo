import { Pipe, PipeTransform } from '@angular/core';
import { Civility } from '../models/models';

@Pipe({
  name: 'civility',
})
export class CivilityPipe implements PipeTransform {
  transform(value: Civility): string {
    if (value === 'm') {
      return 'Monsieur';
    }
    return 'Madame';
  }
}
